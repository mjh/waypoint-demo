# waypoint-demo

Demonstration of using Waypoint in a CI/CD flow

## What this does

* Allows a PR to be opened
* Runs a Waypoint workflow in that PR's "workspace"
* Does the full Waypoint Release